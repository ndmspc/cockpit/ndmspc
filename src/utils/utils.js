import { ProgressVariant } from "@patternfly/react-core";

export const alertTimeout = 500;

export const calculateProgress = (row) => {
    return parseInt(((row[4] + row[5]) / row[6]) * 100);
};

export const progressVariant = (row) => {
    if (row[4] + row[5] < row[6]) {
        return null;
    }
    return row[4] === row[6]
        ? ProgressVariant.success
        : row[5] === row[6]
            ? ProgressVariant.danger
            : ProgressVariant.warning;
};

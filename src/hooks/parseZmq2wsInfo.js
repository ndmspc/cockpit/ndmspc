const parseZmq2wsInfo = (parsed) => {
    return { ...parsed.payload };
};

export default parseZmq2wsInfo;

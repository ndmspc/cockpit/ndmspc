import cockpit from "cockpit";
import * as dayjs from "dayjs";
import parseSalsaData from "./parseSalsaData";
import parseZmq2wsInfo from "./parseZmq2wsInfo";
var relativeTime = require("dayjs/plugin/relativeTime");

dayjs.extend(relativeTime);

let channel = {
    isReady: false,
    error: "",
    source: "",
    isLoading: false,
    channel: null,
    connect: null,
    disconnect: null,
};

const connect = (
    store,
    address = "localhost",
    port = 8442,
    protocol = "ws",
    path = "/"
) => {
    channel = { ...channel, isLoading: true };
    store.channel.setData(channel);
    const ch = cockpit.channel({
        command: "open",
        channel: "a5",
        payload: "websocket-stream1",
        address,
        port: parseInt(port),
        path,
    });
    ch.onready = () => {
        channel = { ...channel, isReady: true, error: "", isLoading: false };
        store.channel.setData(channel);
    };
    ch.onmessage = (data) => {
        const parsed = JSON.parse(data.detail);
        if (parsed.type === "app") {
            store.zmq2wsInfo.setData(parseZmq2wsInfo(parsed));
        } else if (parsed.type === "zmq") {
            console.log(parsed);
            if (parsed.src === 'salsa') {
                store.salsaWs.setData(parseSalsaData(parsed));
            } else if (parsed.src === 'obmon') {
                // TODO: Implement data parsing for OBMON
            }
        }
    };
    ch.onclose = (e, opts) => {
        if (Object.keys(opts).includes("problem")) {
            channel = {
                ...channel,
                error: `Problem connecting to the API at ${protocol}://${address}:${port}.`,
                isReady: false,
            };
        } else {
            channel = {
                ...channel,
                error: "",
                isReady: false,
            };
        }
        store.channel.setData(channel);
    };
    ch.onerror = (e) => {
        channel = {
            ...channel,
            error: e,
            isReady: false
        };
        store.channel.setData(channel);
    };
    channel = {
        ...channel,
        channel: ch
    };
    store.channel.setData(channel);
    return ch;
};

const disconnect = (store, channel) => {
    channel.close();
    store.channel.setData({});
};

export { connect, disconnect };

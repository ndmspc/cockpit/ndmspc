import cockpit from 'cockpit';

const executeCommand = (cmd, setIsExecuted, toolbox, alertTimeout) => () => {
    cockpit.script(`${toolbox ? "/usr/bin/podman exec fedora-toolbox-34 " : ""}/bin/bash -c '${cmd}'`);
    setIsExecuted(true);
    setTimeout(() => setIsExecuted(false), alertTimeout);
};

export default executeCommand;

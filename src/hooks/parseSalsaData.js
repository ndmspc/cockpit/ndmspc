import * as dayjs from "dayjs";

let salsaWs = {
    name: "",
    sub: "",
    node: {},
    jobs: []
};

const parseSalsaData = (parsed) => {
    if (parsed?.data?.data?.jobs === undefined) {
        return salsaWs;
    }
    const sortedJobs = parsed.data.data.jobs.sort(
        (a, b) => b.time.started - a.time.started
    ).map(a => ({
        ...a,
        total: a.P + a.R + a.D + a.F
    }));

    const jobsCount = parsed.data.data.jobs.length;

    const calculatedJobs = parsed.data.data.jobs.reduce(
        (a, c) => {
            const { D, F, P, R } = c;
            if (jobsCount === 1) {
                return [
                    {
                        name: "Total",
                        uid: "",
                        P: a[0].P + P,
                        R: a[0].R + R,
                        D: a[0].D + D,
                        F: a[0].F + F,
                        total: a[0].total + (P + R + D + F),
                        time: c.time,
                    },
                    {
                        name: "Avg",
                        uid: "",
                        P: (a[0].P + P).toFixed(1),
                        R: (a[0].R + R).toFixed(1),
                        D: (a[0].D + D).toFixed(1),
                        F: (a[0].F + F).toFixed(1),
                        total: (a[0].total + (P + R + D + F)).toFixed(1),
                        time: c.time,
                    },
                ];
            }
            return [
                {
                    name: "Total",
                    uid: "",
                    P: a[0].P + P,
                    R: a[0].R + R,
                    D: a[0].D + D,
                    F: a[0].F + F,
                    total: a[0].total + (P + R + D + F),
                    time: c.time,
                },
                {
                    name: "Avg",
                    uid: "",
                    P: (a[0].P / jobsCount).toFixed(1),
                    R: (a[0].R / jobsCount).toFixed(1),
                    D: (a[0].D / jobsCount).toFixed(1),
                    F: (a[0].F / jobsCount).toFixed(1),
                    total: (a[0].total / jobsCount).toFixed(1),
                    time: c.time,
                },
            ];
        },
        [
            {
                name: "Total",
                uid: "",
                P: 0,
                R: 0,
                D: 0,
                F: 0,
                total: 0,
                time: {
                    started: dayjs().unix()
                },
            },
            {
                name: "Avg",
                uid: "",
                P: 0,
                R: 0,
                D: 0,
                F: 0,
                total: 0,
                time: {
                    started: dayjs().unix()
                },
            },
        ]
    );
    salsaWs = {
        ...salsaWs,
        name: parsed.data.name,
        sub: parsed.data.sub,
        node: parsed.data.data.node,
        jobs: [...sortedJobs, ...calculatedJobs].map(
            ({ name, uid, D, F, P, R, total, time: { started } }) => [
                name,
                uid,
                P,
                R,
                D,
                F,
                total,
                dayjs(started * 1000).fromNow(),
            ]
        ),
    };
    return salsaWs;
};

export default parseSalsaData;

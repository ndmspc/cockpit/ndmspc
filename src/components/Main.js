import React from "react";
import cockpit from 'cockpit';
import { NdmSpcBatchSystemComponent } from '@ndmspc/react-ndmspc';

const config = {
    runCommand: (cmd, toolbox) => () => cockpit.script(
        `${
            toolbox ? '/usr/bin/podman exec fedora-toolbox-34 ' : ''
        }/bin/bash -c '${cmd}'`
    ),
    clearFinished: (submitUrl) => () => cockpit.script(`salsa-feeder ${submitUrl} -c "JOB_DEL_FINISHED"`),
    killAll: (submitUrl) => () => cockpit.script(`salsa-feeder ${submitUrl} -c "JOB_DEL_ALL"`)
};

const openConnection = (protocol = 'ws', address = 'localhost', port = 8442, path = '/') => {
    const ch = cockpit.channel({
        command: 'open',
        channel: 'a5',
        payload: 'websocket-stream1',
        address,
        port: parseInt(port),
        path
    });
    ch.parseMessage = data => JSON.parse(data.detail);
    return [ch, "cockpit"];
};

const Main = () => {
    return (
        <NdmSpcBatchSystemComponent config={config} openConnection={openConnection} />
    );
};

export default Main;

import cockpit from 'cockpit';
import React, { useContext, useLayoutEffect, useState } from 'react';
import { JSROOTContext } from "@ndmspc/react-jsroot";
import { useRedirectStoreHook } from '@ndmspc/react-ndmbase';

const NdmspcRootBrowser = ({ store, id = 'test' }) => {
    const status = useContext(JSROOTContext);
    const [filename, setFilename] = useState("");
    const stateFileNameChanged = useRedirectStoreHook(store.fnch);
    useLayoutEffect(() => {
        if (!filename) return;
        if (filename === 'undefined') return;
        if (!filename.endsWith('.root')) return;
        const file = cockpit.file(filename, { binary: true });
        file.watch((content) => {
            if (content === null) return;
            if (status === 'ready') {
                const JSROOT = window.JSROOT;
                console.log(JSROOT);
                JSROOT.require('hierarchy').then(() => {
                    if (!JSROOT.hpainter) {
                        JSROOT.hpainter = new JSROOT.HierarchyPainter('example', id + '-fileBrowser');
                        JSROOT.hpainter.setDisplay('simple', id + '-display');
                    }
                    JSROOT.hpainter.clearHierarchy(true);
                    JSROOT.hpainter.openRootFile(content.buffer).then((file) => {
                        if (file) {
                            console.log(file.h._childs[0]);
                            JSROOT.hpainter.display(file.h._childs[0] ? file.h._childs[0]._name : '', '');
                        }
                    });
                });
            }
        });
        return () => file.close();
    }, [id, status, filename]);

    useLayoutEffect(() => {
        setFilename(stateFileNameChanged.filename);
    }, [stateFileNameChanged]);

    const browserstyle = {
        width: '100%',
        height: 'auto',
        display: 'grid',
        gridTemplateColumns: 'auto 1fr'
    };
    const treestyle = {
    };

    const mystyle = {
        width: '1024px',
        height: '786px'
    };

    return (
        <>
            <div id={id + '-browser'} style={browserstyle}>
                <div id={id + '-fileBrowser'} style={treestyle} />
                <div id={id + '-display'} style={mystyle} />
            </div>
        </>
    );
};

export default NdmspcRootBrowser;

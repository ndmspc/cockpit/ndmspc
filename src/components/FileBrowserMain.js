import React, { useLayoutEffect, useState } from 'react';
import cockpit from 'cockpit';
import { Card, CardTitle, CardBody, InputGroup, TextInput, Button } from '@patternfly/react-core';
import NdmspcRootBrowser from './FileBrowser';
import { redirectStore } from '@ndmspc/react-ndmbase';
const store = {
    fch: redirectStore(),
    fnch: redirectStore()
};

const FileBrowserMain = () => {
    const [filename, setFilename] = useState("");
    const [isDisabled, setIsDisabled] = useState(false);
    useLayoutEffect(() => {
        var promise = cockpit.user();
        promise.then(user => {
            setIsDisabled(false);
            setFilename(`${user.home}/pipe.root`);
        });
    }, []);

    const handleSubmit = () => {
        if (!filename.endsWith('.root')) return;

        if (isDisabled) {
            store.fnch.setData({ filename: null });
        } else {
            console.log(`Filename set to: ${filename}`);
            store.fnch.setData({ filename });
        }
        setIsDisabled(!isDisabled);
    };

    return (
        <Card>
            <CardTitle>ROOT browser</CardTitle>
            <CardBody>
                <InputGroup>
                    <TextInput id="textInput3" aria-label="input with dropdown and button" value={filename} onChange={setFilename} isDisabled={isDisabled} />
                    <Button id="inputDropdownButton1" variant="control" onClick={handleSubmit}>{isDisabled ? "Stop watching" : "Open"}</Button>
                </InputGroup>
                <NdmspcRootBrowser store={store} />
            </CardBody>
        </Card>
    );
};

export default FileBrowserMain;

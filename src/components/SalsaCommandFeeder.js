import React, { useContext } from 'react';
import SalsaCommandJobManager from "./SalsaCommandJobManager";
import {
    Alert,
    FlexItem,
    Checkbox,
    Card,
    CardBody
} from "@patternfly/react-core";
import { SalsaContext } from './SalsaMain';
import { ChannelContext } from "./Main";
import { useRedirectStoreHook } from "@ndmspc/react-ndmbase";
import { alertTimeout } from '../utils/utils';

const SalsaCommandFeeder = () => {
    const { isExecuted, executeCommand, submitUrl, setSubmitUrl, setIsExecuted, toolbox, setToolbox } = useContext(SalsaContext);
    const [cmd, setCmd] = React.useState(`salsa-feeder ${submitUrl} -t "sleep 1:100" --batch`);
    const { store } = useContext(ChannelContext);
    const { node } = useRedirectStoreHook(store.salsaWs);
    const channel = useRedirectStoreHook(store.channel);

    React.useEffect(() => {
        if (!node?.submitUrl) {
            setSubmitUrl("-s tcp://localhost:41000");
            setCmd(`salsa-feeder -s tcp://localhost:41000 -t "sleep 1:100" --batch`);
            return;
        }
        if (`-s ${node.submitUrl}` !== submitUrl) {
            setSubmitUrl(`-s ${node.submitUrl}`);
            setCmd(`salsa-feeder -s ${node.submitUrl} -t "sleep 1:100" --batch`);
        }
    }, [node, node?.submitUrl, submitUrl, setSubmitUrl]);

    if (JSON.stringify(channel) === "{}" || node === undefined) return null;

    if (!isExecuted) {
        return (
            <FlexItem>
                <Card>
                    <CardBody>
                        <>
                            <FlexItem>
                                <SalsaCommandJobManager
                            cmd={cmd}
                            setCmd={setCmd}
                            submitUrl={submitUrl}
                            executeCommand={executeCommand}
                            setIsExecuted={setIsExecuted}
                            toolbox={toolbox}
                                />
                            </FlexItem>
                            <FlexItem>
                                <Checkbox
                            id="run-toolbox"
                            label="Run via toolbox"
                            onChange={setToolbox}
                            isChecked={toolbox}
                                />
                            </FlexItem>
                        </>
                    </CardBody>
                </Card>
            </FlexItem>
        );
    }

    return (
        <FlexItem>
            <Alert
                isInline
                variant="success"
                title="Successfully executed command"
                timeout={alertTimeout}
            />
        </FlexItem>
    );
};

export default SalsaCommandFeeder;

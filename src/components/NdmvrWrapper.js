import React from 'react';
import { Page, PageSection, Title } from '@patternfly/react-core';

const NdmvrWrapper = () => {
    return (
        <Page>
            <PageSection>
                <Title headingLevel="h1">This is wrapper for NDMVR</Title>
            </PageSection>
        </Page>

    );
};

export default NdmvrWrapper;

import React, { useContext } from 'react';
import SalsaTable from "./SalsaTable";
import SalsaHistogramView from "./SalsaHistogramView";
import SalsaCommandFeeder from "./SalsaCommandFeeder";
import { ChannelContext } from "./Main";
import { useRedirectStoreHook } from "@ndmspc/react-ndmbase";
import {
    Radio,
    Flex,
    FlexItem
} from "@patternfly/react-core";

const SalsaViewSelector = () => {
    const [view, setView] = React.useState(0);
    const { store } = useContext(ChannelContext);
    const { node } = useRedirectStoreHook(store.salsaWs);
    const channel = useRedirectStoreHook(store.channel);

    if (Object.keys(channel).length === 0 || !node) {
        return null;
    }

    const handleChange = (_, e) => {
        setView(parseInt(e.currentTarget.value));
    };

    return (
        <>
            <SalsaCommandFeeder />
            <Flex>
                <FlexItem>
                    <Radio
                        isChecked={view === 0}
                        onChange={handleChange}
                        label="Table"
                        id="radio-table"
                        value={0}
                    />
                </FlexItem>
                <FlexItem>
                    <Radio
                        isChecked={view === 1}
                        onChange={handleChange}
                        label="Histogram"
                        id="radio-histogram"
                        value={1}
                    />
                </FlexItem>
            </Flex>
            {view === 0 && <SalsaTable />}
            {view === 1 && <SalsaHistogramView />}
        </>
    );
};

export default SalsaViewSelector;

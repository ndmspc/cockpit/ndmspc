import React, { useContext } from "react";
import {
    Page,
    PageSection,
    Flex,
    FlexItem,
} from "@patternfly/react-core";

import { useRedirectStoreHook } from "@ndmspc/react-ndmbase";
import { ChannelContext } from "./Main";
import executeCommand from "../hooks/executeCommand";
import Zmq2wsServices from "./Zmq2wsServices";
import Zmq2WsInfoCard from "./Zmq2WsInfoCard";
import Zmq2Ws from "./Zmq2Ws";
import SalsaViewSelector from "./SalsaViewSelector";

export const SalsaContext = React.createContext();

const InfoCard = () => {
    const { store } = useContext(ChannelContext);
    const { node } = useRedirectStoreHook(store.salsaWs);
    const { isReady } = useRedirectStoreHook(store.channel);
    if (isReady && node) {
        return (
            <FlexItem>
                <Zmq2WsInfoCard />
            </FlexItem>
        );
    }
    return null;
};

const SalsaMain = () => {
    const { store } = useContext(ChannelContext);
    const { isReady } = useRedirectStoreHook(store.channel);
    const { node } = useRedirectStoreHook(store.salsaWs);
    const [submitUrl, setSubmitUrl] = React.useState(node?.submitUrl ? node.submitUrl : "-s tcp://localhost:41000");
    const [isExecuted, setIsExecuted] = React.useState(false);
    const [toolbox, setToolbox] = React.useState(false);
    // eslint-disable-next-line
    const [ctx, setCtx] = React.useState({
        isExecuted,
        setIsExecuted,
        executeCommand,
        submitUrl,
        setSubmitUrl,
        toolbox,
        setToolbox,
    });

    if (!isReady) {
        return (
            <SalsaContext.Provider value={ctx}>
                <Page>
                    <PageSection>
                        <Flex direction={{ default: "column" }}>
                            <Zmq2Ws />
                        </Flex>
                    </PageSection>
                </Page>
            </SalsaContext.Provider>
        );
    }

    return (
        <SalsaContext.Provider value={ctx}>
            <Page>
                <PageSection>
                    <Flex direction={{ default: "column" }}>
                        <Zmq2Ws />
                        <Zmq2wsServices />
                        <InfoCard />
                        <SalsaViewSelector />
                    </Flex>
                </PageSection>
            </Page>
        </SalsaContext.Provider>
    );
};

export default SalsaMain;
